﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    public GameObject player;
    public PlayerController playerScript;
    Node receiveNode;
    Animator anim;

    public Sprite[] blink;
    public SpriteRenderer sprite;
    public float speed;
    public int Mode; // 0 normal 1 canbeEaten
    public bool isRetreat;
    public bool isNodepresent;
    public bool canR;
    public bool canL;
    public bool canU;
    public bool canD;

    public enum goDirection
    {
        left, right, up, down, none
    }

    public goDirection thisDirection;


    void Awake()
    {
        anim = GetComponent<Animator>();
        sprite = GetComponent<SpriteRenderer>();
        playerScript = player.GetComponent<PlayerController>();
    }
    // Use this for initialization
    void Start()
    {
        thisDirection = goDirection.left;
        isRetreat = false;
        canR = false;
        canL = false;
        canU = false;
        canD = false;
        isNodepresent = false;
    }

    // Update is called once per frame
    void Update()
    {
        ModeAction();
        Movement();
        AnimController();
    }

    void Movement()
    {
        if (thisDirection == goDirection.left)
        {
            if (!canL)
            {
                return;
            }
            else
            {
                transform.Translate(Vector2.left * speed);
            }
        }
        if (thisDirection == goDirection.right)
        {
            if (!canR)
            {
                return;
            }
            else
            {
                transform.Translate(Vector2.right * speed);
            }
        }
        if (thisDirection == goDirection.up)
        {
            if (!canU)
            {
                return;
            }
            else
            {
                transform.Translate(Vector2.up * speed);
            }
        }
        if (thisDirection == goDirection.down)
        {
            if (!canD)
            {
                return;
            }
            else
            {
                transform.Translate(Vector2.down * speed);
            }
        }
    }

    void ModeAction() {
        if (playerScript.Mode == 1) {
            Mode = 1;
        }
        if (playerScript.Mode == 0)
        {
            Mode = 0;
        }
    }
    void AnimController()
    {
        if (Mode == 0)
        {
            isRetreat = false;
            anim.SetBool("retreat", false);
            switch (thisDirection)
            {
                case goDirection.left:
                    anim.SetBool("left", true);
                    anim.SetBool("up", false);
                    anim.SetBool("right", false);
                    anim.SetBool("down", false);
                    break;
                case goDirection.right:
                    anim.SetBool("left", false);
                    anim.SetBool("up", false);
                    anim.SetBool("right", true);
                    anim.SetBool("down", false);
                    break;
                case goDirection.up:
                    anim.SetBool("left", false);
                    anim.SetBool("up", true);
                    anim.SetBool("right", false);
                    anim.SetBool("down", false);
                    break;
                case goDirection.down:
                    anim.SetBool("left", false);
                    anim.SetBool("up", false);
                    anim.SetBool("right", false);
                    anim.SetBool("down", true);
                    break;
            }
        }
        if (Mode == 1){
            isRetreat = true;
            anim.SetBool("retreat",true);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Node"))
        {
            isNodepresent = true;
            receiveNode = other.GetComponent<Node>();
            ReceiveNodeDirection();
            RandomDirection();
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("Node"))
        {
            isNodepresent = true;
            receiveNode = other.GetComponent<Node>();
            ReceiveNodeDirection();
        }
    }

    void ReceiveNodeDirection() {
        if (receiveNode.available[0] == Direction.left)
        {
            canL = true;
        }
        if (receiveNode.available[0] == Direction.none)
        {
            canL = false;
        }
        if (receiveNode.available[1] == Direction.none)
        {
            canR = false;
        }
        if (receiveNode.available[2] == Direction.none)
        {
            canU = false;
        }
        if (receiveNode.available[3] == Direction.none)
        {
            canD = false;
        }
        if (receiveNode.available[1] == Direction.right)
        {
            canR = true;
        }
        if (receiveNode.available[2] == Direction.up)
        {
            canU = true;
        }
        if (receiveNode.available[3] == Direction.down)
        {
            canD = true;
        }
    }

    void RandomDirection() {
        if (isNodepresent) {
            if (Mode == 0)
            {
                int rd = Random.Range(1, 5);
                if (rd == 1)
                {
                    if (!canL)
                    {
                        thisDirection = goDirection.right;
                    }
                    else
                    {
                        thisDirection = goDirection.left;
                    }
                }
                if (rd == 2)
                {
                    if (!canR)
                    {
                        thisDirection = goDirection.left;
                    }
                    else
                    {
                        thisDirection = goDirection.right;
                    }
                }
                if (rd == 3)
                {
                    if (!canU)
                    {
                        thisDirection = goDirection.down;
                    }
                    else
                    {
                        thisDirection = goDirection.up;
                    }
                }
                if (rd == 4)
                {
                    if (!canD)
                    {
                        thisDirection = goDirection.up;
                    }
                    else
                    {
                        thisDirection = goDirection.down;
                    }
                }
            }
            else if (Mode == 1) {
                if (playerScript.thisDirection == PlayerController.goDirection.left) {
                    if (canR)
                    {
                        thisDirection = goDirection.right;
                    }
                    else if (canU)
                    {
                        thisDirection = goDirection.up;
                    }
                    else if (canD)
                    {
                        thisDirection = goDirection.down;
                    }
                }
                if (playerScript.thisDirection == PlayerController.goDirection.right) {
                    if (canL)
                    {
                        thisDirection = goDirection.left;
                    }
                    else if (canU)
                    {
                        thisDirection = goDirection.up;
                    }
                    else if (canD)
                    {
                        thisDirection = goDirection.down;
                    }
                }
                if (playerScript.thisDirection == PlayerController.goDirection.up) {
                    if (canU)
                    {
                        thisDirection = goDirection.up;
                    }
                    else if (canL)
                    {
                        thisDirection = goDirection.left;
                    }
                    else if (canR)
                    {
                        thisDirection = goDirection.right;
                    }
                }
                if (playerScript.thisDirection == PlayerController.goDirection.up)
                {
                    if (canD)
                    {
                        thisDirection = goDirection.down;
                    }
                    else if (canR)
                    {
                        thisDirection = goDirection.right;
                    }
                    else if (canL)
                    {
                        thisDirection = goDirection.left;
                    }
                }
            }
        }
    }

    void OnTriggerExit2D(Collider2D other) {
        if (other.CompareTag("Node")) {
            isNodepresent = false;
        }
    }
}
