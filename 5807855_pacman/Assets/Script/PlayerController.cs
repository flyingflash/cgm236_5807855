﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    Node receiveNode;
    Animator anim;

    public float speed;
    public float eattimer;
    public int Mode; // 0 Normal 1 Eat 2 Die
    public bool isNodepresent;
    public bool canR;
    public bool canL;
    public bool canU;
    public bool canD;

    public enum goDirection {
        left , right , up , down , none
    }

    public goDirection thisDirection;


    void Awake() {
        anim = GetComponent<Animator>();
    }
	// Use this for initialization
	void Start () {
        thisDirection = goDirection.left;
        Mode = 0;
        eattimer = 7;
        canR = false;
        canL = false;
        canU = false;
        canD = false;
        isNodepresent = false;
        StartCoroutine(DeathDestroy());
    }
	
	// Update is called once per frame
	void Update () {
        ModeAction();
        KeyInput();
        Movement();
        AnimController();
	}

    void ModeAction() {
        if (Mode == 0) {
            eattimer = 7;
            return;
        }
        else if (Mode == 1) {
            eattimer -= Time.smoothDeltaTime;
            if (eattimer <= 0) {
                Mode = 0;
            }
        }
    }

    IEnumerator DeathDestroy() {
        while (true) {
            yield return new WaitUntil(() => Mode == 2);
            yield return new WaitForSeconds(anim.GetCurrentAnimatorStateInfo(0).length + anim.GetCurrentAnimatorStateInfo(0).normalizedTime);
            Destroy(gameObject);
            break;
        }
    }

    void Movement() {
        if (thisDirection == goDirection.left) {
            if (!canL)
            {
                return;
            }
            else
            {
                transform.Translate(Vector2.left * speed);
            }
        }
        if (thisDirection == goDirection.right)
        {
            if (!canR)
            {
                return;
            }
            else
            {
                transform.Translate(Vector2.right * speed);
            }
        }
        if (thisDirection == goDirection.up)
        {
            if (!canU)
            {
                return;
            }
            else
            {
                transform.Translate(Vector2.up * speed);
            }
        }
        if (thisDirection == goDirection.down)
        {
            if (!canD)
            {
                return;
            }
            else
            {
                transform.Translate(Vector2.down * speed);
            }
        }
    }

    void KeyInput() {
        if (isNodepresent)
        {
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                if (Vector3.Distance(transform.position, receiveNode.gameObject.transform.position) < 0.08f) {
                    transform.position = receiveNode.gameObject.transform.position;
                }
                if (canL)
                {
                    thisDirection = goDirection.left;
                }
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                if (Vector3.Distance(transform.position, receiveNode.gameObject.transform.position) < 0.08f)
                {
                    transform.position = receiveNode.gameObject.transform.position;
                }
                if (canR)
                {
                    thisDirection = goDirection.right;
                }
            }
            else if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                if (Vector3.Distance(transform.position, receiveNode.gameObject.transform.position) < 0.08f)
                {
                    transform.position = receiveNode.gameObject.transform.position;
                }
                if (canU)
                {
                    thisDirection = goDirection.up;
                }
            }
            else if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                if (Vector3.Distance(transform.position, receiveNode.gameObject.transform.position) < 0.08f)
                {
                    transform.position = receiveNode.gameObject.transform.position;
                }
                if (canD)
                {
                    thisDirection = goDirection.down;
                }
            }
        }
        else if (!isNodepresent) {
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                switch (thisDirection) {
                    case goDirection.left:
                        break;
                    case goDirection.right:
                        thisDirection = goDirection.left;
                        break;
                }
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                switch (thisDirection)
                {
                    case goDirection.left:
                        thisDirection = goDirection.right;
                        break;
                    case goDirection.right:
                        break;
                }
            }
            else if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                switch (thisDirection)
                {
                    case goDirection.up:
                        break;
                    case goDirection.down:
                        thisDirection = goDirection.up;
                        break;
                }
            }
            else if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                switch (thisDirection)
                {
                    case goDirection.up:
                        thisDirection = goDirection.down;
                        break;
                    case goDirection.down:
                        break;
                }
            }
        }
    }

    void AnimController() {
        if (Mode == 0 || Mode == 1)
        {
            switch (thisDirection)
            {
                case goDirection.left:
                    anim.SetBool("left", true);
                    anim.SetBool("up", false);
                    anim.SetBool("right", false);
                    anim.SetBool("down", false);
                    break;
                case goDirection.right:
                    anim.SetBool("left", false);
                    anim.SetBool("up", false);
                    anim.SetBool("right", true);
                    anim.SetBool("down", false);
                    break;
                case goDirection.up:
                    anim.SetBool("left", false);
                    anim.SetBool("up", true);
                    anim.SetBool("right", false);
                    anim.SetBool("down", false);
                    break;
                case goDirection.down:
                    anim.SetBool("left", false);
                    anim.SetBool("up", false);
                    anim.SetBool("right", false);
                    anim.SetBool("down", true);
                    break;
            }
        }
        else {
            anim.SetTrigger("death");
        }
    }

    void OnTriggerEnter2D(Collider2D other) {
        if (other.CompareTag("Dots")) {
            Destroy(other.gameObject);
        }
        if (other.CompareTag("Dot")) {
            Mode = 1;
            Destroy(other.gameObject);
        }
        if (other.CompareTag("Enemy")) {
            if (Mode == 1) {
                Destroy(other.gameObject);
            }
            if (Mode == 0) {
                Mode = 2;
            }
        }
        if (other.CompareTag("Node"))
        {
            isNodepresent = true;
            receiveNode = other.GetComponent<Node>();
        }
    }

    void OnTriggerStay2D(Collider2D other) {
        if (other.CompareTag("Node")) {
            isNodepresent = true;
            receiveNode = other.GetComponent<Node>();
            if (receiveNode.available[0] == Direction.left) {
                canL = true;
            }
            if (receiveNode.available[0] == Direction.none)
            {
                canL = false;
            }
            if (receiveNode.available[1] == Direction.none)
            {
                canR = false;
            }
            if (receiveNode.available[2] == Direction.none)
            {
                canU = false;
            }
            if (receiveNode.available[3] == Direction.none)
            {
                canD = false;
            }
            if (receiveNode.available[1] == Direction.right)
            {
                canR = true;
            }
            if (receiveNode.available[2] == Direction.up)
            {
                canU = true;
            }
            if (receiveNode.available[3] == Direction.down)
            {
                canD = true;
            }
        }
    }

    void OnTriggerExit2D(Collider2D other) {
        if (other.CompareTag("Node")) {
            isNodepresent = false;
            if (receiveNode.available[0] == Direction.none)
            {
                canL = true;
            }
            if (receiveNode.available[1] == Direction.none)
            {
                canR = true;
            }
            if (receiveNode.available[2] == Direction.none)
            {
                canU = true;
            }
            if (receiveNode.available[3] == Direction.none)
            {
                canD = true;
            }
        }
    }
}
