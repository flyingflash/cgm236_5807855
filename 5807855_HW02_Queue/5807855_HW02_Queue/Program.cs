﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5807855_HW02_Queue
{
    class Program
    {
        static void Main(string[] args)
        {
            CQueue myQueue = new CQueue();
            string choice, value;

            string word = "Subin";

            for (int i = 0; i < word.Length; i++) {
                myQueue.Enqueue(word.Substring(i, 1));
            }

            while (true){
                Console.WriteLine("-----------------------------");
                Console.WriteLine("(f) front");
                Console.WriteLine("(e) enquene");
                Console.WriteLine("(d) dequeue");
                Console.WriteLine("(c) count");

                choice = Console.ReadLine();
                choice = choice.ToLower();

                char[] onechar = choice.ToCharArray();

                switch (onechar[0]) {
                    case 'f' :
                        Console.WriteLine("Front : " + myQueue.Front());
                        break;
                    case 'd':
                        myQueue.Dequeue();
                        Console.WriteLine("Dequeue");
                        break;
                    case 'e':
                        Console.WriteLine();
                        Console.WriteLine("Enter value to Enqueue: ");
                        value = Console.ReadLine();
                        myQueue.Enqueue(value);
                        Console.WriteLine("Enqueue : " + value.ToString());
                        break;
                    case 'c':
                        Console.WriteLine("Count : " + myQueue.Count);
                        break;
                }
            }
        }
    }
}
