﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace _5807855_HW02_Queue
{
    public class CQueue
    {
        ArrayList pqueue;

        public CQueue() {
            pqueue = new ArrayList();
        }

        public void Enqueue(Object item) {
            pqueue.Add(item);
        }

        public void Dequeue() {
            pqueue.RemoveAt(0);
        }

        public Object Front() {
            return pqueue[0];
        }

        public int Count {
            get { return pqueue.Count; }
        }

        public void ClearQueue() {
            pqueue.Clear();
        }
    }
}
