﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5807855_Closet_BruteForce
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true) {
                points[] p;
                double best = double.MaxValue;
                Console.WriteLine("\nPress any key to random points\n");
                Console.Read();
                p = new points[12];
                for (int i = 0; i < p.Length; i++)
                {
                    p[i] = new points();
                    Console.WriteLine("Point " + (i+1) + " : " + p[i].ShowPos());
                }
                Console.WriteLine("\nPress any key to find the closet pair");
                Console.ReadKey();
                int Ci = 0;
                int Cj = 0;
                for (int i = 0; i < p.Length - 1; i++)
                {
                    for (int j = 0; j < p.Length - 2; j++)
                    {
                        double distance;
                        distance = Math.Sqrt(((p[j].x - p[i].x) * (p[j].x - p[i].x)) + ((p[j].y - p[i].y) * (p[j].y - p[i].y)));
                        if (distance < best && distance != 0)
                        {
                            best = distance;
                            Ci = i;
                            Cj = j;
                            //Console.WriteLine("Range betwwen point " + i + " and " + j + " is " + Convert.ToInt32(best));
                        }
                    }
                }
                Console.WriteLine("\nCloset Pair is point " + (Ci+1) + " and point " + (Cj+1) + " with nearest distance of " + Convert.ToInt32(best));
                Console.ReadKey();
            }
        }
    }
}
