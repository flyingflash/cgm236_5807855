﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace _5807855_HW02_Stack
{
    public class CStack
    {
        private int p_index;
        private ArrayList list;

        public CStack() {
            list = new ArrayList();
            p_index = -1;
        }

        public int Count {
            get { return list.Count; }
        }

        public void Push(Object item) {
            list.Add(item);
            p_index++;
        }

        public Object Pop()
        {
            Object obj = list[p_index];
            list.RemoveAt(p_index);
            p_index--;
            return obj;
        }

        public void Clear() {
            list.Clear();
        }

        public Object Top() {
            return list[p_index];
        }
    }
}
