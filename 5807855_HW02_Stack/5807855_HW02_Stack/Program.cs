﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5807855_HW02_Stack
{
    class Program
    {
        static void Main(string[] args)
        {
            CStack myStack = new CStack();
            string choice, value;
            string word = "Skywalker";

            for (int i = 0; i < word.Length; i++) {
                myStack.Push(word.Substring(i, 1));
            }

            while (true) {
                Console.WriteLine("----------------------------");
                Console.WriteLine("(t) Top");
                Console.WriteLine("(o) pop");
                Console.WriteLine("(u) push");
                Console.WriteLine("(c) count");
                Console.WriteLine("(x) clear");

                choice = Console.ReadLine();
                choice = choice.ToLower();

                char[] onechar = choice.ToCharArray();

                switch (onechar[0]) {
                    case 't':
                        Console.WriteLine("Top : " + myStack.Top()) ;
                        break;
                    case 'o':
                        Console.WriteLine("Remove Top : " + myStack.Top());
                        myStack.Pop();
                        Console.WriteLine("Current Top is : " + myStack.Top());
                        break;
                    case 'u':
                        Console.WriteLine();
                        Console.WriteLine("Enter value to push :");
                        value = Console.ReadLine();
                        myStack.Push(value);
                        Console.WriteLine("Pushed : " + value.ToString());
                        break;
                    case 'c':
                        Console.WriteLine("Count : " + myStack.Count);
                        break;
                    case 'x':
                        Console.WriteLine("Stack has been cleared");
                        break;
                }
            }
        }
    }
}
