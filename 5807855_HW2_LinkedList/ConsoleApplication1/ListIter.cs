﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    public class ListIter
    {
        private Node current;
        private Node previous;
        LinkedList theList;

        public ListIter(LinkedList list)
        {
            theList = list;
            current = theList.GetFirst();
            previous = null;
        }

        public void NextLink()
        {
            previous = current;
            current = current.Link;
        }

        public Node GetCurrent()
        {
            return current;
        }

        public void InsertAfter(Object theElement)
        {
            Node newNode = new Node(theElement);
            newNode.Link = current.Link;
            current.Link = newNode;
            NextLink();
        }

        public void Remove()
        {
            previous.Link = current.Link;
        }


        public void Reset()
        {
            current = theList.GetFirst();
            previous = null;
        }

        public bool AtEnd()
        {
            return (current.Link == null);
        }
    }
}
