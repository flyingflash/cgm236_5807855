﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            LinkedList myList = new LinkedList();
            ListIter iter = new ListIter(myList);

            string choice, value;

            iter.InsertAfter("01");
            iter.InsertAfter("02");
            iter.InsertAfter("0vvvvvvv2");

            while (true)
            {
                Console.WriteLine("----------------------");
                Console.WriteLine("(n) move to next node");
                Console.WriteLine("(g) get value in current node");
                Console.WriteLine("(f) reset iterator");
                Console.WriteLine("(s) show complete list");
                Console.WriteLine("(a) Insert After");

                choice = Console.ReadLine();
                choice = choice.ToLower();

                char[] onechar = choice.ToCharArray();

                switch (onechar[0]) {
                    case 'n':
                        if ((!myList.IsEmpty()) && (!(iter.AtEnd())))
                        {
                            iter.NextLink();
                        }
                        else {
                            Console.WriteLine("Can yall move to the next link");
                        }
                        break;

                    case 'g':
                        if (!myList.IsEmpty())
                        {
                            Console.WriteLine("Element: " + iter.GetCurrent().Element);
                        }
                        else {
                            Console.WriteLine("List is Empty");
                        }
                        break;
                    case 'f':
                        iter.Reset();
                        break;
                    case 'r':
                        iter.Remove();
                        break;
                    case 's':
                        if (!(myList.IsEmpty()))
                        {
                            myList.ShowList();
                        }
                        else {
                            Console.WriteLine("List is Empty");
                        }
                        break;
                    case 'a':
                        Console.WriteLine();
                        Console.Write("Enter value to insert:");
                        value = Console.ReadLine();
                        iter.InsertAfter(value);
;                        break;
                }
            }
        }
    }
}
