﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class Form1 : Form
    {
        List<int> b = new List<int>();
        int[] a;
        Random rnd = new Random();
        int second;
        public Form1()
        {
            InitializeComponent();
            CreateArray();
            label1.Text = string.Join(" ", a.Select(p => p.ToString()).ToArray());
        }

        private void button2_Click(object sender, EventArgs e)
        {
            CreateArray();
            label1.Text = string.Join(" ", a.Select(p => p.ToString()).ToArray());
        }

        void CreateArray() {
            label1.Text = "";
            a = new int[10];
            for (int i = 0; i < a.Length; i++){
                a[i] = rnd.Next(0, 100);
            }
        }

        void BSorting() {
            label1.Text = "";
            for (int i = 0; i < a.Length; i++) {
                label1.Text += a[i] + " ";
                for (int j = 0; j < a.Length - 1; j++)
                {
                    if (a[j] > a[j + 1])
                    {
                        int temp = a[j + 1];
                        a[j + 1] = a[j];
                        a[j] = temp;
                        second = 0;
                    }
                }
            }
        }

        void SSorting() {
            int min, temp;
            for (int i = 0; i < a.Length; i++) {
                min = i;
                for (int j = i + 1; j < a.Length; j++)
                {
                    if (a[j] < a[min])
                    {
                        min = j;
                    }
                }
                if (min != i)
                {
                    temp = a[i];
                    a[i] = a[min];
                    a[min] = temp;
                }
            }
        }

        void ISorting() {
            int inner, temp;
            for (int i = 1; i < a.Length - 1; i++) {
                temp = a[i];
                inner = i - 1;
                while (inner >= 0 && a[inner] > temp) {
                    a[inner + 1] = a[inner];
                    inner = inner - 1;
                }
                a[inner + 1] = temp;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label1.Text = "";
            timer1.Start();
            BSorting();
            label1.Text = string.Join(" ", a.Select(p => p.ToString()).ToArray());
        }

        private void button3_Click(object sender, EventArgs e)
        {
            label1.Text = "";
            SSorting();
            label1.Text = string.Join(" ", a.Select(p => p.ToString()).ToArray());
        }

        private void button4_Click(object sender, EventArgs e)
        {
            label1.Text = "";
            ISorting();
            label1.Text = string.Join(" ", a.Select(p => p.ToString()).ToArray());
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
