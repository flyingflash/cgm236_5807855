﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5807855_HW02_Graph
{
    class Program
    {
        static void Main(string[] args)
        {
            Graph myGraph = new Graph();
            myGraph.AddVertex("A");
            myGraph.AddVertex("B");
            myGraph.AddVertex("C");

            myGraph.AddEdge(0, 1);
            myGraph.AddEdge(1, 2);

            myGraph.ShowVertex(2);

            Console.Read();
        }
    }
}
